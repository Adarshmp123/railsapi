Rails.application.routes.draw do 
  resource :patients
  resource :doctors
  resource :appointments
  namespace :api, defaults:{ format: :json }do
    namespace :v1 do
      resources :patients, :only => [:show, :create, :index]
      resources :doctors, :only => [:show, :create, :index]
      resources :appointments, :only => [:show, :create, :index]
    end
  end 
end 