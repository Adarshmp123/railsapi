class RemovePatNameFromAppointments < ActiveRecord::Migration[5.0]
  def change
    remove_column :appointments, :pat_name
  end
end
