class CreateAppointments < ActiveRecord::Migration[5.0]
  def change
    create_table :appointments do |t|
      t.string :doc_name
      t.string :pat_name
      t.string :diseases
      t.integer :patient_id
      t.integer :doctor_id

      t.timestamps
    end
  end
end
