class Api::V1::AppointmentsController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:index, :show, :create]
  respond_to :json

  def index
    @appointments = Appointment.all
    arr = []
    @appointments.each do |appointment|
    arr << {
      id: appointment.id, 
      diseases: appointment.diseases, 
      patient: Patient.find(appointment.patient_id).name,
      doctor: Doctor.find(appointment.doctor_id).name
    }
    end
    render json: { success: true, appointments: arr }
  end

  def show
    respond_with Appointment.find(params[:id])
  end

  def create
    @appointment=Appointment.new(appointment_params) 
    if @appointment.save 
      render json: @appointment, status: 201
    else
      render json: { errors: @appointment.errors}, status: 422
    end
  end

  private
  def appointment_params
    params.require(:appointment).permit(:diseases, :patient_id, :doctor_id) 
  end

end  